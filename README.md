[![pipeline status](https://gitlab.com/advprog.c12/studyhelper-tools/badges/master/pipeline.svg)](https://gitlab.com/advprog.c12/studyhelper-tools/-/commits/master)
[![coverage report](https://gitlab.com/advprog.c12/studyhelper-tools/badges/master/coverage.svg)](https://gitlab.com/advprog.c12/studyhelper-tools/-/commits/master)

[![studyhelper bot](https://img.shields.io/badge/-StudyHelper%20Bot-blue.svg?logo=discord&logoColor=ffffff)](https://gitlab.com/advprog.c12/study-helper-bot)

# StudyHelper Tools (StudyHelper Bot Microservice)

StudyHelper Tools is a service for the StudyHelper Bot.

## Services

### Calculator

Helps the students to calculate things such as arithmetics and statistics. Students can calculate their tasks using this feature.

### Randomizer

Helps to create groups for group projects with random people that are registered in the server randomly or based on certain filters.

## Developers

| Name                               | NPM        | GitLab                                                | PIC                    |
| ---------------------------------- | ---------- | ----------------------------------------------------- | ---------------------- |
| Gabriel Enrique                    | 1906293032 | [gabriel.enrique](https://gitlab.com/gabriel.enrique) | Chat Archive           |
| Lazuardi P.P. Nusantara            | 1906398225 | [ardinusantara](https://gitlab.com/ardinusantara)     | Study Material Storage |
| Kaysa Syifa Wijdan Amin            | 1906400362 | [kaysaswa](https://gitlab.com/kaysaswa)               | Randomizer             |
| Mario Serano                       | 1906398484 | [mario_serano](https://gitlab.com/mario_serano)       | Schoolworks Reminder   |
| Matthew T.P.                       | 1906308500 | [matthewsolomon_](https://gitlab.com/matthewsolomon_) | Calculator             |

## Contributing

#### 1. Clone this repository

```shell
git clone https://gitlab.com/advprog.c12/studyhelper-tools.git
```

#### 2. Contribute

You can start contributing right away
