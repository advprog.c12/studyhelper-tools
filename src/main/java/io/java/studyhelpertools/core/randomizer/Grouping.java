package io.java.studyhelpertools.core.randomizer;

import java.util.List;

public class Grouping {
    private Strategy strategy;

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public String[] executeStrategy(List<String> membersList, int a) {
        return strategy.execute(membersList,a);
    }
}
