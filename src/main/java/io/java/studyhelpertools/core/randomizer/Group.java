package io.java.studyhelpertools.core.randomizer;

import java.util.Collections;
import java.util.List;

public class Group implements Strategy {
    private boolean isShuffled;

    @Override
    public String[] execute(List<String> membersList, int a) {
        int numberOfNames = membersList.size();
        int numberOfGroup = a;
        int numberPerGroup = numberOfNames / numberOfGroup;
        boolean ag = numberOfNames % numberOfGroup > 0;

        if (ag) {
            numberPerGroup++;
        }
        String[] en = new String[numberOfNames];
        shuffle(membersList);
        membersList.toArray(en);
        StringBuilder sb = new StringBuilder();
        int j = 0;
        int g = 0;
        for (int i = 0;i < numberOfGroup;i++) {
            for (;j < en.length;j++) {
                sb.append(en[j] + "\n");
                g++;
                if (g == numberPerGroup) {
                    g = 0;
                    j++;
                    break;
                }
            }
            sb.append("#");
        }
        String[] eg = sb.toString().split("#");
        return eg;
    }

    @Override
    public List<String> shuffle(List<String> c) {
        Collections.shuffle(c);
        isShuffled = true;
        return c;
    }

    @Override
    public String getType() {
        return "group";
    }
}
