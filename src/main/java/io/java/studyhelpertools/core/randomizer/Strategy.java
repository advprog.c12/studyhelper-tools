package io.java.studyhelpertools.core.randomizer;

import java.util.List;

public interface Strategy {
    String[] execute(List<String> membersList, int a);

    String getType();

    List<String> shuffle(List<String> c);
}
