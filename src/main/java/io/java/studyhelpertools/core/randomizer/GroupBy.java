package io.java.studyhelpertools.core.randomizer;

import java.util.Collections;
import java.util.List;

public class GroupBy implements Strategy {
    private boolean isShuffled;

    @Override
    public String[] execute(List<String> membersList, int numberEachGroup) {
        int numberOfNames = membersList.size();
        int num = numberEachGroup;
        int numberGroups = numberOfNames / num;
        boolean check = numberOfNames % num > 0;

        if (check) {
            numberGroups++;
        }
        String[] members = new String[membersList.size()];
        shuffle(membersList);
        membersList.toArray(members);
        StringBuilder sb = new StringBuilder();
        int j = 0;
        int g = 0;
        for (int i = 0;i < numberGroups;i++) {
            for (;j < members.length;j++) {
                sb.append(members[j] + "\n");
                g++;
                if (g == num) {
                    g = 0;
                    j++;
                    break;
                }
            }
            sb.append("#");
        }
        String[] eg = sb.toString().split("#");
        return eg;
    }

    @Override
    public List<String> shuffle(List<String> c) {
        Collections.shuffle(c);
        isShuffled = true;
        return c;
    }

    @Override
    public String getType() {
        return "groupby";
    }

}
