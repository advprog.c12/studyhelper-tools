package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class Cosinus implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.cos(number);
    }

    @Override
    public String toBeReturned(double number) {
        if (number == 90.00) {
            return "0";
        }

        double returned = this.doCalculate(number);
        return Double.toString(returned);
    }
}
