package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class Cotangent implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.tan(number);
    }

    @Override
    public String toBeReturned(double number) {
        if (number == 90.00) {
            return "Undefined";
        }

        double returned = this.doCalculate(number);

        if (returned == 0) {
            return "Undefined";
        }

        return Double.toString(returned);
    }
}
