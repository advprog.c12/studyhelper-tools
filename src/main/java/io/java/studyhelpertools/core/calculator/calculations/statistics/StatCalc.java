package io.java.studyhelpertools.core.calculator.calculations.statistics;

import java.util.List;

public interface StatCalc {

    int doCalculate(int [] dataList);

    String toBeReturned(List<String> args);
}
