package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public interface TrigonoCalc {

    double doCalculate(double number);

    String toBeReturned(double number);
}
