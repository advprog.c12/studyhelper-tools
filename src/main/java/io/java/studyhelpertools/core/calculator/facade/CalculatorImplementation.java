package io.java.studyhelpertools.core.calculator.facade;

import io.java.studyhelpertools.core.calculator.calculations.arithmetic.*;

public class CalculatorImplementation {

    private Addition addition;
    private Subtraction subtraction;
    private Multiplication multiplication;
    private Division division;
    private Modulo modulo;

    public CalculatorImplementation() {
        this.addition = new Addition();
        this.subtraction = new Subtraction();
        this.multiplication = new Multiplication();
        this.division = new Division();
        this.modulo = new Modulo();
    }

    public String add(int number1, int number2) {
        return addition.toBeReturned(number1,number2);
    }

    public String sub(int number1, int number2) {
        return subtraction.toBeReturned(number1,number2);
    }

    public String mul(int number1, int number2) {
        return multiplication.toBeReturned(number1,number2);
    }

    public String div(int number1, int number2) {
        return division.toBeReturned(number1,number2);
    }

    public String mod(int number1, int number2) {
        return modulo.toBeReturned(number1,number2);
    }
}
