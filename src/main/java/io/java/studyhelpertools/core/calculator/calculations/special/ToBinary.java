package io.java.studyhelpertools.core.calculator.calculations.special;

public class ToBinary implements SpecCalc {

    @Override
    public double doCalculate(double x) {
        return Double.valueOf(String.valueOf(Integer.toBinaryString((int) x)));
    }

    @Override
    public String toBeReturned(double x) {
        double returned = this.doCalculate(x);
        return Double.toString(returned);
    }
}
