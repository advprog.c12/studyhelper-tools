package io.java.studyhelpertools.core.calculator.calculations.statistics;

import java.util.List;

public class Mean implements StatCalc {

    @Override
    public int doCalculate(int[] dataList) {
        int totalValue = 0;

        for (int i = 0; i < dataList.length; i++) {
            totalValue += dataList[i];
        }

        int toBeReturned = totalValue / dataList.length;
        return toBeReturned;
    }

    @Override
    public String toBeReturned(List<String> args) {
        int [] dataList = new int[args.size()];

        for (int i = 0; i < args.size(); i++) {
            dataList[i] = Integer.parseInt(args.get(i));
        }

        int toReturn = this.doCalculate(dataList);
        return Integer.toString(toReturn);
    }
}
