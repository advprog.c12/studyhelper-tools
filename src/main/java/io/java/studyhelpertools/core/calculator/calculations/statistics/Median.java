package io.java.studyhelpertools.core.calculator.calculations.statistics;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Median implements StatCalc {

    @Override
    public int doCalculate(int[] dataList) {
        Arrays.sort(dataList);
        int returned = 0;

        if (dataList.length % 2 == 0) {
            returned = dataList[dataList.length / 2] + dataList[(dataList.length - 1) / 2];
            return returned / 2;
        }

        returned = dataList[(dataList.length) / 2];
        return returned;
    }

    @Override
    public String toBeReturned(List<String> args) {
        Collections.sort(args);
        int [] dataList = new int[args.size()];

        for (int i = 0; i < args.size(); i++) {
            dataList[i] = Integer.parseInt(args.get(i));
        }

        int toReturn = this.doCalculate(dataList);
        return Integer.toString(toReturn);
    }
}
