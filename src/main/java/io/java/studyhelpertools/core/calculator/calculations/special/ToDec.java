package io.java.studyhelpertools.core.calculator.calculations.special;

public class ToDec implements SpecCalc {

    @Override
    public double doCalculate(double x) {
        int decimal = 0;
        int n = 0;

        while (true) {
            if (x == 0) {
                break;
            } else {
                int temp = (int) (x % 10);
                decimal += temp * Math.pow(2, n);
                x = x / 10;
                n++;
            }
        }

        return decimal;
    }

    @Override
    public String toBeReturned(double x) {
        double returned = this.doCalculate(x);
        return Double.toString(returned);
    }
}
