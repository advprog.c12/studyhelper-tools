package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class Tangent implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.tan(number);
    }

    @Override
    public String toBeReturned(double number) {
        if (number == 90.00) {
            return "Undefined";
        }

        double returned = this.doCalculate(number);
        return Double.toString(returned);
    }
}
