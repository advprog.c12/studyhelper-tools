package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class Secant implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.cos(number);
    }

    @Override
    public String toBeReturned(double number) {
        if (number == 90.00 || number == 270.00) {
            return "Undefined";
        }

        double returned = this.doCalculate(number);

        return Double.toString(1 / returned);
    }
}
