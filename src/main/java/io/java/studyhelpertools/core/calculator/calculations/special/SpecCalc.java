package io.java.studyhelpertools.core.calculator.calculations.special;

public interface SpecCalc {

    double doCalculate(double x);

    String toBeReturned(double x);
}
