package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class Sinus implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.sin(number);
    }

    @Override
    public String toBeReturned(double number) {
        double returned = this.doCalculate(number);
        return Double.toString(returned);
    }
}
