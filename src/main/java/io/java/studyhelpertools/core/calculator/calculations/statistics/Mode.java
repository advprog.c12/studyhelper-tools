package io.java.studyhelpertools.core.calculator.calculations.statistics;

import java.util.List;

public class Mode implements StatCalc {

    @Override
    public int doCalculate(int[] dataList) {
        int maxValue = 0;
        int maxCount = 0;
        int n = dataList.length;

        for (int i = 0; i < n; ++i) {
            int count = 0;

            for (int j = 0; j < n; ++j) {
                if (dataList[j] == dataList[i]) {
                    ++count;
                }
            }

            if (count > maxCount) {
                maxCount = count;
                maxValue = dataList[i];
            }
        }
        return maxValue;
    }

    @Override
    public String toBeReturned(List<String> args) {
        int[] dataList = new int[args.size()];

        for (int i = 0; i < args.size(); i++) {
            dataList[i] = Integer.parseInt(args.get(i));
        }

        int toReturn = this.doCalculate(dataList);
        return Integer.toString(toReturn);
    }
}
