package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class Cosecant implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.sin(number);
    }

    @Override
    public String toBeReturned(double number) {
        double returned = this.doCalculate(number);

        if (returned == 0) {
            return "Undefined";
        }

        return Double.toString(returned);
    }
}
