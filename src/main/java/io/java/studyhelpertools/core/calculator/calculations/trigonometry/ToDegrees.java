package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

public class ToDegrees implements TrigonoCalc {

    @Override
    public double doCalculate(double number) {
        return Math.toDegrees(number);
    }

    @Override
    public String toBeReturned(double number) {
        double returned = this.doCalculate(number);
        return Double.toString(returned);
    }
}
