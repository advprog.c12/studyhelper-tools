package io.java.studyhelpertools.core.calculator.facade;

import io.java.studyhelpertools.core.calculator.calculations.statistics.*;
import java.util.List;

public class StatCalcImplementation {

    private Mode mode;
    private Mean mean;
    private Median median;

    public StatCalcImplementation() {
        this.mode = new Mode();
        this.mean = new Mean();
        this.median = new Median();
    }

    public String findMode(List<String> args) {
        return this.mode.toBeReturned(args);
    }

    public String findMean(List<String> args) {
        return this.mean.toBeReturned(args);
    }

    public String findMedian(List<String> args) {
        return this.median.toBeReturned(args);
    }
}
