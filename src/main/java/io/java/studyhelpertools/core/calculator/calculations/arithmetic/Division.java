package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

public class Division implements Calculate {

    @Override
    public int doCalculate(int number1, int number2) {
        return number1 / number2;
    }

    @Override
    public String toBeReturned(int number1, int number2) {
        int toReturn = this.doCalculate(number1, number2);
        return Integer.toString(toReturn);
    }
}
