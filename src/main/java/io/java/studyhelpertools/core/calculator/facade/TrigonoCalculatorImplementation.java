package io.java.studyhelpertools.core.calculator.facade;

import io.java.studyhelpertools.core.calculator.calculations.trigonometry.*;

public class TrigonoCalculatorImplementation {

    private Cosecant cosecant;
    private Cosinus cosinus;
    private Cotangent cotangent;
    private Secant secant;
    private Sinus sinus;
    private Tangent tangent;
    private ToDegrees toDegrees;
    private ToRadians toRadians;

    public TrigonoCalculatorImplementation() {
        this.cosecant = new Cosecant();
        this.cosinus = new Cosinus();
        this.cotangent = new Cotangent();
        this.secant = new Secant();
        this.sinus = new Sinus();
        this.tangent = new Tangent();
        this.toDegrees = new ToDegrees();
        this.toRadians = new ToRadians();
    }

    public String csc(double number) {
        return cosecant.toBeReturned(number);
    }

    public String cos(double number) {
        return cosinus.toBeReturned(number);
    }

    public String cot(double number) {
        return cotangent.toBeReturned(number);
    }

    public String sec(double number) {
        return secant.toBeReturned(number);
    }

    public String sin(double number) {
        return sinus.toBeReturned(number);
    }

    public String tan(double number) {
        return tangent.toBeReturned(number);
    }

    public String toDegree(double number) {
        return toDegrees.toBeReturned(number);
    }

    public String toRad(double number) {
        return toRadians.toBeReturned(number);
    }
}
