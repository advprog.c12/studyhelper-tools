package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

public interface Calculate {

    int doCalculate(int number1, int number2);

    String toBeReturned(int number1, int number2);
}
