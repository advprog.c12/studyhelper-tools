package io.java.studyhelpertools.core.calculator.facade;

import io.java.studyhelpertools.core.calculator.calculations.special.*;

public class SpecialCalcImplementation {

    private CNDF cndf;
    private ToBinary tobinary;
    private ToDec todec;

    public SpecialCalcImplementation() {
        this.cndf = new CNDF();
        this.tobinary = new ToBinary();
        this.todec = new ToDec();
    }

    public String cndf(double number) {
        return this.cndf.toBeReturned(number);
    }

    public String toBinary(double number) {
        return this.tobinary.toBeReturned(number);
    }

    public String toDecimal(double number) {
        return this.todec.toBeReturned(number);
    }
}
