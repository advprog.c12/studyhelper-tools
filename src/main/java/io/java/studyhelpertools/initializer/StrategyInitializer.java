package io.java.studyhelpertools.initializer;

import io.java.studyhelpertools.core.randomizer.Group;
import io.java.studyhelpertools.core.randomizer.GroupBy;
import io.java.studyhelpertools.repository.randomizer.StrategyRepository;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StrategyInitializer {
    @Autowired
    private StrategyRepository strategyRepository;

    @PostConstruct
    public void init() {
        this.strategyRepository.addStrategy(new GroupBy());
        this.strategyRepository.addStrategy(new Group());
    }
}
