package io.java.studyhelpertools.repository.randomizer;

import io.java.studyhelpertools.model.randomizer.Anggota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnggotaRepository extends JpaRepository<Anggota,String> {
}
