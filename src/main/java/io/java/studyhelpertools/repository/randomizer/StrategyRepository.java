package io.java.studyhelpertools.repository.randomizer;

import io.java.studyhelpertools.core.randomizer.Strategy;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyRepository {
    private Map<String, Strategy> strategies = new HashMap<>();

    public Iterable<Strategy> getStrategy() {
        return strategies.values();
    }

    public void addStrategy(Strategy strategy) {
        this.strategies.put(strategy.getType(), strategy);
    }

    public Strategy getStrategyByType(String type) {
        return strategies.get(type);
    }
}
