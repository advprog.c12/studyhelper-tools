package io.java.studyhelpertools.service;

import io.java.studyhelpertools.model.SampleModel;
import io.java.studyhelpertools.repository.SampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleServiceImpl implements SampleService {

    @Autowired
    private SampleRepository sampleRepository;

    @Override
    public SampleModel createSampleModel(SampleModel sampleModel) {
        sampleRepository.save(sampleModel);
        return sampleModel;
    }

    @Override
    public SampleModel getSampleModelById(String id) {
        return sampleRepository.findBySampleId(id);
    }
}
