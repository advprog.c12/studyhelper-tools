package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.core.calculator.facade.StatCalcImplementation;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    private StatCalcImplementation implementation = new StatCalcImplementation();

    @Override
    public CalculatorResult findMode(List<String> args) {
        String result = implementation.findMode(args);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult findMean(List<String> args) {
        String result = implementation.findMean(args);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult findMedian(List<String> args) {
        String result = implementation.findMedian(args);
        return new CalculatorResult(result);
    }
}
