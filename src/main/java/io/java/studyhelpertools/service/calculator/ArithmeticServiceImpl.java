package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.core.calculator.facade.CalculatorImplementation;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import org.springframework.stereotype.Service;

@Service
public class ArithmeticServiceImpl implements ArithmeticService {

    private CalculatorImplementation implementation = new CalculatorImplementation();

    @Override
    public CalculatorResult add(int number1, int number2) {
        String result = implementation.add(number1, number2);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult sub(int number1, int number2) {
        String result = implementation.sub(number1, number2);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult mul(int number1, int number2) {
        String result = implementation.mul(number1, number2);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult div(int number1, int number2) {
        String result = implementation.div(number1, number2);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult mod(int number1, int number2) {
        String result = implementation.mod(number1, number2);
        return new CalculatorResult(result);
    }
}
