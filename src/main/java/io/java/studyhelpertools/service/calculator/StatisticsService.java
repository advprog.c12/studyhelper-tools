package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import java.util.List;

public interface StatisticsService {

    CalculatorResult findMode(List<String> args);

    CalculatorResult findMean(List<String> args);

    CalculatorResult findMedian(List<String> args);
}
