package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.core.calculator.facade.TrigonoCalculatorImplementation;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import org.springframework.stereotype.Service;

@Service
public class TrigonometryServiceImpl implements TrigonometryService {

    private TrigonoCalculatorImplementation implementation = new TrigonoCalculatorImplementation();

    @Override
    public CalculatorResult csc(double number) {
        String result = implementation.csc(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult cos(double number) {
        String result = implementation.cos(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult cot(double number) {
        String result = implementation.cot(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult sec(double number) {
        String result = implementation.sec(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult sin(double number) {
        String result = implementation.sin(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult tan(double number) {
        String result = implementation.tan(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult toDegree(double number) {
        String result = implementation.toDegree(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult toRad(double number) {
        String result = implementation.toRad(number);
        return new CalculatorResult(result);
    }
}
