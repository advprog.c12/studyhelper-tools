package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;

public interface SpecialService {

    CalculatorResult cndf(double number);

    CalculatorResult toBinary(double number);

    CalculatorResult toDecimal(double number);
}