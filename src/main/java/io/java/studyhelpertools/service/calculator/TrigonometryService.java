package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;

public interface TrigonometryService {

    CalculatorResult csc(double number);

    CalculatorResult cos(double number);

    CalculatorResult cot(double number);

    CalculatorResult sec(double number);

    CalculatorResult sin(double number);

    CalculatorResult tan(double number);

    CalculatorResult toDegree(double number);

    CalculatorResult toRad(double number);
}
