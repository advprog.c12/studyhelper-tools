package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;

public interface ArithmeticService {

    CalculatorResult add(int number1, int number2);

    CalculatorResult sub(int number1, int number2);

    CalculatorResult mul(int number1, int number2);

    CalculatorResult div(int number1, int number2);

    CalculatorResult mod(int number1, int number2);
}
