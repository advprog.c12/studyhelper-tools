package io.java.studyhelpertools.service.calculator;

import io.java.studyhelpertools.core.calculator.facade.SpecialCalcImplementation;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import org.springframework.stereotype.Service;

@Service
public class SpecialServiceImpl implements SpecialService {

    private SpecialCalcImplementation implementation = new SpecialCalcImplementation();

    @Override
    public CalculatorResult cndf(double number) {
        String result = implementation.cndf(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult toBinary(double number) {
        String result = implementation.toBinary(number);
        return new CalculatorResult(result);
    }

    @Override
    public CalculatorResult toDecimal(double number) {
        String result = implementation.toDecimal(number);
        return new CalculatorResult(result);
    }
}
