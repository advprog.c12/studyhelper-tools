package io.java.studyhelpertools.service.randomizer;

import io.java.studyhelpertools.core.randomizer.Strategy;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import java.util.List;

public interface GroupingService {
    String[] chooseStrategy(String type, List<String> membersList, int jumlahGroup);

    String toString(String[] membersList);

    Iterable<Strategy> getStrategies();

    RandomizerResult grouping(String strategyChoosen,int jumlahGroup);
}
