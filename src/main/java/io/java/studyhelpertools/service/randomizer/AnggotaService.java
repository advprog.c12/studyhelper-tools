package io.java.studyhelpertools.service.randomizer;

import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import java.util.List;

public interface AnggotaService {
    Anggota createAnggota(String name);

    List<Anggota> getListAnggota();

    void deleteAllAnggota();

    RandomizerResult register(List<String> args);

    RandomizerResult delete();

    RandomizerResult show();
}
