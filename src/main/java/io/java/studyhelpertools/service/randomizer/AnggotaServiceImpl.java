package io.java.studyhelpertools.service.randomizer;

import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.repository.randomizer.AnggotaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnggotaServiceImpl implements AnggotaService {

    @Autowired
    private AnggotaRepository anggotaRepository;

    @Override
    public Anggota createAnggota(String name) {
        Anggota anggota = new Anggota(name);
        anggotaRepository.save(anggota);
        return anggota;
    }

    @Override
    public List<Anggota> getListAnggota() {
        return anggotaRepository.findAll();
    }

    @Override
    public void deleteAllAnggota() {
        anggotaRepository.deleteAll();
    }

    @Override
    public RandomizerResult register(List<String> args) {
        String result = "";
        String response = "List of anggota : \n ";

        if (getListAnggota().isEmpty()) {
            String hasil = "Succesfully add list of anggota \n ";
            for (String a : args) {
                createAnggota(a);
                response += a + "\n";

            }
            result = hasil + response;

        } else {
            String hasil1 = "There is list of anggota listed \n ";
            for (Anggota a : getListAnggota()) {
                response += a.getName() + "\n";
            }
            result = hasil1 + response;
        }
        return new RandomizerResult(result);
    }

    @Override
    public RandomizerResult delete() {
        String response1 = "";
        String response = "List of anggota : \n ";
        String result = "";

        List<Anggota> anggota = getListAnggota();

        if (anggota.size() == 0) {
            response1 += "There is no list of anggota listed";
            result = response1;
        } else {
            for (Anggota a : getListAnggota()) {
                response += a.getName() + "\n";
            }
            deleteAllAnggota();
            response1 += "Succesfully delete list of anggota \n ";
            result = response1 + response;

        }
        return new RandomizerResult(result);

    }

    @Override
    public RandomizerResult show() {
        String response1 = "";
        String response = "List of anggota : \n ";
        String result = "";

        List<Anggota> anggota = getListAnggota();

        if (anggota.size() == 0) {
            response1 += "There is no list of anggota listed";
            result = response1;
        } else {
            for (Anggota a : getListAnggota()) {
                response += a.getName() + "\n";
            }
            result = response;

        }
        return new RandomizerResult(result);

    }
}
