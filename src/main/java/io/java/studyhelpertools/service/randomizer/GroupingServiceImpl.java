package io.java.studyhelpertools.service.randomizer;

import io.java.studyhelpertools.core.randomizer.Grouping;
import io.java.studyhelpertools.core.randomizer.Strategy;
import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.repository.randomizer.StrategyRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class GroupingServiceImpl implements GroupingService {
    private final StrategyRepository strategyRepository;

    private AnggotaService anggotaService;

    public GroupingServiceImpl(StrategyRepository strategyRepository, AnggotaService anggotaService) {
        this.strategyRepository = strategyRepository;
        this.anggotaService = anggotaService;
    }

    @Override
    public String[] chooseStrategy(String strategyType, List<String> membersList, int a) {
        Grouping grouping = new Grouping();
        Strategy strategy = strategyRepository.getStrategyByType(strategyType);
        grouping.setStrategy(strategy);
        String[] hasil = grouping.executeStrategy(membersList,a);
        return hasil;
    }

    @Override
    public String toString(String[] membersList) {
        String output = "";
        int gn = 1;
        for (String group:membersList) {
            String[] n = group.split(" ");
            output += "Group " + gn + "\n";
            for (String group1:n) {
                output += group1;
            }
            gn++;
            output += "\n";
        }
        return output;
    }

    @Override
    public Iterable<Strategy> getStrategies() {
        return strategyRepository.getStrategy();
    }

    @Override
    public RandomizerResult grouping(String strategyChoosen, int jumlahGroup) {
        String response = "";

        List<String> members = new ArrayList<>();

        List<Anggota> anggota = anggotaService.getListAnggota();

        if (anggota.size() == 0) {
            response += "Please register list of members first using command --register "
                    + "<list_of_names_with_enter_to_separate_each_names>";
        } else {
            for (Anggota anggota1 : anggota) {
                members.add(anggota1.getName());
            }

            String[] hasil = chooseStrategy(strategyChoosen,members,jumlahGroup);

            response += toString(hasil);
        }
        return new RandomizerResult(response);
    }
}

