package io.java.studyhelpertools.service;

import io.java.studyhelpertools.model.SampleModel;

public interface SampleService {

    SampleModel createSampleModel(SampleModel sampleModel);

    SampleModel getSampleModelById(String id);

}
