package io.java.studyhelpertools.controller.randomizer;

import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.AnggotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/randomizer")
public class DeleteController {
    @Autowired
    private AnggotaService anggotaService;

    @GetMapping(path = "/delete", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity delete() {
        RandomizerResult result = anggotaService.delete();
        return ResponseEntity.ok(result);
    }
}
