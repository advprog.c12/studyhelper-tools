package io.java.studyhelpertools.controller.randomizer;

import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.AnggotaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/randomizer")
public class RegisterController {
    @Autowired
    private AnggotaService anggotaService;

    @GetMapping(path = "/register/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity register(@RequestParam List<String> values) {
        RandomizerResult result = anggotaService.register(values);
        return ResponseEntity.ok(result);
    }
}
