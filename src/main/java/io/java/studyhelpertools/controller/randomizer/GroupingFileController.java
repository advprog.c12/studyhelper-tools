package io.java.studyhelpertools.controller.randomizer;

import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.GroupingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/randomizer")
public class GroupingFileController {
    @Autowired
    private GroupingService groupingService;

    @GetMapping(path = "/groupingfile/{a}/{b}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity groupingfile(@PathVariable(value = "a") String a, @PathVariable(value = "b") Integer b) {
        RandomizerResult result = groupingService.grouping(a,b);
        return ResponseEntity.ok(result);
    }
}
