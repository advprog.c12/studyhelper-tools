package io.java.studyhelpertools.controller.randomizer;

import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.AnggotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/randomizer")
public class ShowListController {
    @Autowired
    private AnggotaService anggotaService;

    @GetMapping(path = "/showlist", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity show() {
        RandomizerResult result = anggotaService.show();
        return ResponseEntity.ok(result);
    }
}
