package io.java.studyhelpertools.controller.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.SpecialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/calculator/special")
public class SpecialController {

    @Autowired
    private SpecialService specialService;

    @GetMapping(path = "/cndf/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity cndf(@PathVariable(value = "a") String a) {
        CalculatorResult result = specialService.cndf(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/toBinary/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity toBinary(@PathVariable(value = "a") String a) {
        CalculatorResult result = specialService.toBinary(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/toDecimal/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity toDecimal(@PathVariable(value = "a") String a) {
        CalculatorResult result = specialService.toDecimal(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }
}