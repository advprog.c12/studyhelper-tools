package io.java.studyhelpertools.controller.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.ArithmeticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/calculator/arithmetic")
public class ArithmeticController {

    @Autowired
    private ArithmeticService arithmeticService;

    @GetMapping(path = "/add/{a}/{b}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addition(@PathVariable(value = "a") String a, @PathVariable(value = "b") String b) {
        CalculatorResult result = arithmeticService.add(Integer.parseInt(a),Integer.parseInt(b));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/sub/{a}/{b}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity subtraction(@PathVariable(value = "a") String a, @PathVariable(value = "b") String b) {
        CalculatorResult result = arithmeticService.sub(Integer.parseInt(a),Integer.parseInt(b));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/mul/{a}/{b}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity multiplication(@PathVariable(value = "a") String a, @PathVariable(value = "b") String b) {
        CalculatorResult result = arithmeticService.mul(Integer.parseInt(a),Integer.parseInt(b));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/div/{a}/{b}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity division(@PathVariable(value = "a") String a, @PathVariable(value = "b") String b) {
        CalculatorResult result = arithmeticService.div(Integer.parseInt(a),Integer.parseInt(b));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/mod/{a}/{b}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity modulo(@PathVariable(value = "a") String a, @PathVariable(value = "b") String b) {
        CalculatorResult result = arithmeticService.mod(Integer.parseInt(a),Integer.parseInt(b));
        return ResponseEntity.ok(result);
    }
}
