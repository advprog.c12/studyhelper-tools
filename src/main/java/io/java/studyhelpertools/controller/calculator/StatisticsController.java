package io.java.studyhelpertools.controller.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.StatisticsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/calculator/statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @GetMapping(path = "/mean/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity mean(@RequestParam List<String> values) {
        CalculatorResult result = statisticsService.findMean(values);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/median/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity median(@RequestParam List<String> values) {
        CalculatorResult result = statisticsService.findMedian(values);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/mode/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity mode(@RequestParam List<String> values) {
        CalculatorResult result = statisticsService.findMode(values);
        return ResponseEntity.ok(result);
    }
}
