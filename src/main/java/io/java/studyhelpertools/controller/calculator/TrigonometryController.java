package io.java.studyhelpertools.controller.calculator;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.TrigonometryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/calculator/trigonometry")
public class TrigonometryController {

    @Autowired
    private TrigonometryService trigonometryService;

    @GetMapping(path = "/csc/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity csc(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.csc(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/cos/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity cos(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.cos(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/cot/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity cot(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.cot(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/sec/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity sec(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.sec(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/sin/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity sin(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.sin(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/tan/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity tan(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.tan(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/toDegree/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity toDegree(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.toDegree(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/toRad/{a}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity toRad(@PathVariable(value = "a") String a) {
        CalculatorResult result = trigonometryService.toRad(Double.parseDouble(a));
        return ResponseEntity.ok(result);
    }
}
