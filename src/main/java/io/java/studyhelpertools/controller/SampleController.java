package io.java.studyhelpertools.controller;

import io.java.studyhelpertools.model.SampleModel;
import io.java.studyhelpertools.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/sample")
public class SampleController {

    @Autowired
    private SampleService sampleService;

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getSample(@PathVariable(value = "id") String id) {
        SampleModel sampleModel = sampleService.getSampleModelById(id);

        if (sampleModel == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(sampleModel);
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postSample(@RequestBody SampleModel sampleModel) {
        return ResponseEntity.ok(sampleService.createSampleModel(sampleModel));
    }
}
