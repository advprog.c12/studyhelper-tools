package io.java.studyhelpertools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyHelperToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyHelperToolsApplication.class, args);
    }
}
