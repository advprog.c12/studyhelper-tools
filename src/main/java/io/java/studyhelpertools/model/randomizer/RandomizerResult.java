package io.java.studyhelpertools.model.randomizer;

import lombok.Data;

@Data
public class RandomizerResult {
    private String result;

    public RandomizerResult(String result) {
        this.result = result;
    }

}
