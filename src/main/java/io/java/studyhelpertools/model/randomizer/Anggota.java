package io.java.studyhelpertools.model.randomizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "anggota")
@Data
@NoArgsConstructor
public class Anggota {

    @Id
    @Column(name = "name", updatable = false, nullable = false)
    private String name;

    public Anggota(String name) {
        this.name = name;
    }
}
