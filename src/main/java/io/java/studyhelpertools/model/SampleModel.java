package io.java.studyhelpertools.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "sample")
@Data
@NoArgsConstructor
public class SampleModel {

    @Id
    @Column(name = "sample_id", updatable = false)
    private String sampleId;

    @Column(name = "data_string")
    private String dataString;

    public SampleModel(String sampleId, String dataString) {
        this.sampleId = sampleId;
        this.dataString = dataString;
    }
}
