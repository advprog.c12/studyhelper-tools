package io.java.studyhelpertools.model.calculator;

import lombok.Data;

@Data
public class CalculatorResult {

    private String result;

    public CalculatorResult(String result) {
        this.result = result;
    }
}
