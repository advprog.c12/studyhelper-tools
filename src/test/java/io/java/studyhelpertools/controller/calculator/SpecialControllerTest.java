package io.java.studyhelpertools.controller.calculator;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.SpecialServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = SpecialController.class)
public class SpecialControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SpecialServiceImpl specialService;

    private String baseUrl = "/calculator/special";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerCndf() throws Exception {
        CalculatorResult expected = new CalculatorResult(Double.toString(1.0));
        when(specialService.cndf(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/cndf/45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("1.0"));
    }

    @Test
    public void testControllerToBinary() throws Exception {
        CalculatorResult expected = new CalculatorResult(Double.toString(101101.0));
        when(specialService.toBinary(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/toBinary/45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("101101.0"));
    }

    @Test
    public void testControllerToDecimal() throws Exception {
        CalculatorResult expected = new CalculatorResult(Double.toString(45.0));
        when(specialService.toDecimal(101101)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/toDecimal/101101")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("45.0"));
    }
}
