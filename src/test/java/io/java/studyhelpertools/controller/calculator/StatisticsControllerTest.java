package io.java.studyhelpertools.controller.calculator;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.StatisticsServiceImpl;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = StatisticsController.class)
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StatisticsServiceImpl statisticsService;

    private String baseUrl = "/calculator/statistics";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String valuesParser(List<String> arr) {
        return String.join(",", arr);
    }

    @Test
    public void testControllerMean() throws Exception {
        List<String> values = Arrays.asList(new String[]{"1", "2", "3", "4", "5"});
        int mean = (1 + 2 + 3 + 4 + 5) / 5;

        CalculatorResult expected = new CalculatorResult(Integer.toString(mean));
        when(statisticsService.findMean(values)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/mean/?values=" + valuesParser(values))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(Integer.toString(mean)));
    }

    @Test
    public void testControllerMedian() throws Exception {
        List<String> values = Arrays.asList(new String[]{"1", "2", "3", "4", "5"});
        int median = 3;

        CalculatorResult expected = new CalculatorResult(Integer.toString(median));
        when(statisticsService.findMedian(values)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/median/?values=" + valuesParser(values))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(Integer.toString(median)));
    }

    @Test
    public void testControllerMode() throws Exception {
        List<String> values = Arrays.asList(new String[]{"1", "1", "1", "3", "3"});
        int mode = 1;

        CalculatorResult expected = new CalculatorResult(Integer.toString(mode));
        when(statisticsService.findMode(values)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/mode/?values=" + valuesParser(values))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(Integer.toString(mode)));
    }
}
