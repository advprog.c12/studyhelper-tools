package io.java.studyhelpertools.controller.calculator;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.ArithmeticServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = ArithmeticController.class)
public class ArithmeticControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ArithmeticServiceImpl arithmeticService;

    private String baseUrl = "/calculator/arithmetic";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerAddition() throws Exception {
        CalculatorResult expected = new CalculatorResult(Integer.toString(10 + 9));
        when(arithmeticService.add(10, 9)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/add/10/9")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("19"));
    }

    @Test
    public void testControllerSubtraction() throws Exception {
        CalculatorResult expected = new CalculatorResult(Integer.toString(29 - 10));
        when(arithmeticService.sub(29, 10)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/sub/29/10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("19"));
    }

    @Test
    public void testControllerMultiplication() throws Exception {
        CalculatorResult expected = new CalculatorResult(Integer.toString(2 * 333));
        when(arithmeticService.mul(2, 333)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/mul/2/333")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("666"));
    }

    @Test
    public void testControllerDivision() throws Exception {
        CalculatorResult expected = new CalculatorResult(Integer.toString(1332 / 2));
        when(arithmeticService.div(1332, 2)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/div/1332/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("666"));
    }

    @Test
    public void testControllerModulo() throws Exception {
        CalculatorResult expected = new CalculatorResult(Integer.toString(2 % 3));
        when(arithmeticService.mod(2, 3)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/mod/2/3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("2"));
    }
}
