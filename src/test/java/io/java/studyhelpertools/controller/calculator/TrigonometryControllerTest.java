package io.java.studyhelpertools.controller.calculator;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.calculator.CalculatorResult;
import io.java.studyhelpertools.service.calculator.TrigonometryServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = TrigonometryController.class)
public class TrigonometryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TrigonometryServiceImpl trigonometryService;

    private String baseUrl = "/calculator/trigonometry";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerCsc() throws Exception {
        String result = "0.8509035245341184";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.csc(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/csc/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerCos() throws Exception {
        String result = "0.5253219888177297";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.cos(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/cos/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerCot() throws Exception {
        String result = "1.6197751905438615";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.cot(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/cot/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerSec() throws Exception {
        String result = "0.5253219888177297";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.sec(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/sec/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerSin() throws Exception {
        String result = "0.8509035245341184";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.sin(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/sin/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerTan() throws Exception {
        String result = "1.6197751905438615";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.tan(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/tan/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerToDegree() throws Exception {
        String result = "2578.3100780887044";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.toDegree(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/toDegree/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerToRad() throws Exception {
        String result = "0.7853981633974483";

        CalculatorResult expected = new CalculatorResult(result);
        when(trigonometryService.toRad(45)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/toRad/" + "45")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }
}
