package io.java.studyhelpertools.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.SampleModel;
import io.java.studyhelpertools.service.SampleServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = SampleController.class)
public class SampleControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SampleServiceImpl sampleService;

    private String baseUrl = "/sample";
    private SampleModel sampleModel;

    @BeforeEach
    public void setUp() {
        sampleModel = new SampleModel("ID-00", "sample data");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetSample() throws Exception {
        when(sampleService.getSampleModelById(sampleModel.getSampleId())).thenReturn(sampleModel);

        mvc.perform(get(baseUrl + "/ID-00")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sampleId").value("ID-00"))
                .andExpect(jsonPath("$.dataString").value("sample data"));
    }

    @Test
    public void testControllerGetSampleNotFound() throws Exception {
        when(sampleService.getSampleModelById(anyString())).thenReturn(null);

        mvc.perform(get(baseUrl + "/ID-01")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerPostSample() throws Exception {
        when(sampleService.createSampleModel(any(SampleModel.class))).thenReturn(sampleModel);

        mvc.perform(post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(sampleModel)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sampleId").value("ID-00"))
                .andExpect(jsonPath("$.dataString").value("sample data"));
    }
}
