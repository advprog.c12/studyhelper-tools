package io.java.studyhelpertools.controller.randomizer;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.AnggotaService;
import io.java.studyhelpertools.service.randomizer.GroupingServiceImpl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = GroupingFileController.class)
public class GroupingFileControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnggotaService anggotaService;

    @MockBean
    private GroupingServiceImpl groupingService;

    private String baseUrl = "/randomizer";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGroupingFile() throws Exception {
        String response = "";

        String strategyType = "group";

        int jumlah = 3;

        List<String> values = Arrays.asList(new String[]{"A","B","C"});

        for (String a : values) {
            anggotaService.createAnggota(a);
        }
        List<String> members = new ArrayList<>();

        List<Anggota> anggota = anggotaService.getListAnggota();

        for (Anggota anggota1 : anggota) {
            members.add(anggota1.getName());
        }

        String[] hasil = groupingService.chooseStrategy(strategyType,members,jumlah);

        response += groupingService.toString(hasil);

        RandomizerResult expected = new RandomizerResult(response);

        when(groupingService.grouping(strategyType,jumlah)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/groupingfile/group/3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(response));
    }

    @Test
    public void testControllerGroupingFileIfListAnggotaKosong() throws Exception {
        String response = "";

        String strategyType = "group";

        int jumlah = 3;

        response += "Please register list of members first using command --register "
                + "<list_of_names_with_enter_to_separate_each_names>";

        RandomizerResult expected = new RandomizerResult(response);

        when(groupingService.grouping(strategyType,jumlah)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/groupingfile/group/3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(response));
    }
}
