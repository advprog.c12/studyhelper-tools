package io.java.studyhelpertools.controller.randomizer;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.AnggotaServiceImpl;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = RegisterController.class)
public class RegisterControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnggotaServiceImpl anggotaService;

    private String baseUrl = "/randomizer";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String valuesParser(List<String> arr) {
        return String.join(",", arr);
    }

    @Test
    public void testControllerRegister() throws Exception {
        String result = "";

        String response = "List of anggota : \n ";

        List<String> values = Arrays.asList(new String[]{"A","B","C"});

        String hasil = "Succesfully add list of anggota \n ";
        for (String a : values) {
            anggotaService.createAnggota(a);
            response += a + "\n";

        }
        result = hasil + response;

        RandomizerResult expected = new RandomizerResult(result);
        when(anggotaService.register(values)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/register/?values=" + valuesParser(values))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }

    @Test
    public void testControllerRegisterIfListNotEmpty() throws Exception {
        String result = "";

        String response = "List of anggota : \n ";

        List<String> values = Arrays.asList(new String[]{"A","B","C"});

        for (String a : values) {
            anggotaService.createAnggota(a);
        }

        String hasil1 = "There is list of anggota listed \n ";
        for (Anggota a : anggotaService.getListAnggota()) {
            response += a.getName() + "\n";
        }
        result = hasil1 + response;

        RandomizerResult expected = new RandomizerResult(result);
        when(anggotaService.register(values)).thenReturn(expected);

        mvc.perform(get(baseUrl + "/register/?values=" + valuesParser(values))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result));
    }
}
