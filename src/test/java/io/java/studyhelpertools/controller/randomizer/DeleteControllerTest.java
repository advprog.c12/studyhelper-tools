package io.java.studyhelpertools.controller.randomizer;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.service.randomizer.AnggotaServiceImpl;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = DeleteController.class)
public class DeleteControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnggotaServiceImpl anggotaService;

    private String baseUrl = "/randomizer";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerDelete() throws Exception {
        String response = "";
        String response1 = "";
        String result1 = "";

        List<String> values = Arrays.asList(new String[]{"A","B","C"});

        for (String a : values) {
            anggotaService.createAnggota(a);
        }

        List<Anggota> anggota = anggotaService.getListAnggota();

        for (Anggota a : anggota) {
            response += a.getName() + "\n";
        }
        response1 += "Succesfully delete list of anggota \n ";

        result1 = response1 + response;

        RandomizerResult expected = new RandomizerResult(result1);

        when(anggotaService.delete()).thenReturn(expected);

        mvc.perform(get(baseUrl + "/delete")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result1));
    }

    @Test
    public void testControllerDeleteIfListEmpty() throws Exception {
        String response1 = "";
        String result1 = "";

        response1 += "There is no list of anggota listed";
        result1 = response1;

        RandomizerResult result = new RandomizerResult(result1);

        when(anggotaService.delete()).thenReturn(result);

        mvc.perform(get(baseUrl + "/delete")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value(result1));
    }
}
