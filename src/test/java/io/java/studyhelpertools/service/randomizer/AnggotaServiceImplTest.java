package io.java.studyhelpertools.service.randomizer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import io.java.studyhelpertools.model.randomizer.Anggota;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.repository.randomizer.AnggotaRepository;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AnggotaServiceImplTest {
    @Mock
    private AnggotaRepository anggotaRepository;

    @InjectMocks
    private AnggotaServiceImpl anggotaService;

    private Anggota anggota;

    private Class<?> serviceClass;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        String serviceName = "io.java.studyhelpertools.service.randomizer.AnggotaServiceImpl";
        serviceClass = Class.forName(serviceName);
        anggota = new Anggota();
        anggota.setName("a");
    }

    @Test
    public void testServiceCreateAnggota() {
        lenient().when(anggotaService.createAnggota("a"))
                .thenReturn(anggota);

        assertEquals(anggotaService.createAnggota("a").getName(),anggota.getName());

        verify(anggotaRepository,times(1)).save(anggota);

    }

    @Test
    public void testServiceGetListAnggota() {
        List<Anggota> anggotas = new ArrayList<>();
        anggotas.add(anggota);
        when(anggotaService.getListAnggota()).thenReturn(anggotas);
        lenient().when(anggotaRepository.findAll()).thenReturn(anggotas);

        assertEquals(anggotaRepository.findAll(),anggotas);

        verify(anggotaRepository,times(1)).findAll();
    }

    @Test
    public void testServiceDeleteAnggota() {
        List<Anggota> anggotas = new ArrayList<>();
        anggotas.add(anggota);
        anggotaService.deleteAllAnggota();

        assertTrue(anggotaRepository.findAll().size() == 0);

        verify(anggotaRepository,times(1)).deleteAll();
    }

    @Test
    public void testServiceHasCreateAnggotaMethod() throws Exception {
        Method executeStrategy = serviceClass.getDeclaredMethod("createAnggota",
                String.class);

        assertTrue(Modifier.isPublic(executeStrategy.getModifiers()));
        assertEquals(1, executeStrategy.getParameterCount());
        assertEquals("io.java.studyhelpertools.model.randomizer.Anggota",
                executeStrategy.getGenericReturnType().getTypeName());
    }

    @Test
    public void testServiceHasGetListAnggotaMethod() throws Exception {
        Method executeStrategy = serviceClass.getDeclaredMethod("getListAnggota");

        assertTrue(Modifier.isPublic(executeStrategy.getModifiers()));
        assertEquals(0, executeStrategy.getParameterCount());
        assertEquals("java.util.List<io.java.studyhelpertools.model.randomizer.Anggota>",
                executeStrategy.getGenericReturnType().getTypeName());
    }

    @Test
    public void testServiceHasDeleteAllAnggotaMethod() throws Exception {
        Method executeStrategy = serviceClass.getDeclaredMethod("deleteAllAnggota");

        assertTrue(Modifier.isPublic(executeStrategy.getModifiers()));
        assertEquals(0, executeStrategy.getParameterCount());
        assertEquals("void", executeStrategy.getGenericReturnType().getTypeName());
    }


    @Test
    public void testServiceOnRegisterWhenListEmpty() throws Exception {
        List<String> values = Arrays.asList(new String[]{"A","B","C"});

        RandomizerResult expected = new RandomizerResult("Succesfully add list of anggota \n "
                + "List of anggota : \n a\nb\nc\n");
        lenient().when(anggotaService.register(values)).thenReturn(expected);
    }

    @Test
    public void testServiceOnRegisterWhenListNotEmpty() throws Exception {
        List<String> values = Arrays.asList(new String[]{"A","B","C"});

        anggotaService.register(values);

        List<String> values2 = Arrays.asList(new String[]{"D","E","F"});

        RandomizerResult expected = new RandomizerResult("There is list of anggota listed \n "
                + "List of anggota : \n a\nb\nc\n");
        lenient().when(anggotaService.register(values2)).thenReturn(expected);
    }

    @Test
    public void testServiceOnDeleteWhenListIsEmpty() throws Exception {
        RandomizerResult expected = new RandomizerResult("There is no list of anggota listed");
        assertEquals(expected,anggotaService.delete());
    }

    @Test
    public void testServiceOnShowListWhenListIsEmpty() throws Exception {
        RandomizerResult expected = new RandomizerResult("There is no list of anggota listed");
        assertEquals(expected,anggotaService.show());
    }
}
