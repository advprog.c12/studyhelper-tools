package io.java.studyhelpertools.service.randomizer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import io.java.studyhelpertools.core.randomizer.Group;
import io.java.studyhelpertools.core.randomizer.Grouping;
import io.java.studyhelpertools.core.randomizer.Strategy;
import io.java.studyhelpertools.model.randomizer.RandomizerResult;
import io.java.studyhelpertools.repository.randomizer.StrategyRepository;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GroupingServiceImplTest {
    @Mock
    private Grouping grouping;

    @Mock
    private StrategyRepository strategyRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private Strategy strategy;

    @InjectMocks
    private GroupingServiceImpl groupingService;

    @BeforeEach
    public void setUp() {
        groupingService = new GroupingServiceImpl(strategyRepository,anggotaService);
        strategy = new Group();
    }

    @Test
    public void testServiceOnChooseStrategy() throws Exception {
        String strategyType = "group";

        lenient().when(strategyRepository.getStrategyByType(strategyType)).thenReturn(strategy);

        grouping.setStrategy(strategy);

        List<String> values = Arrays.asList(new String[]{"A","B","C"});
        int numberOfGroup = 2;

        String[] hasil = groupingService.chooseStrategy(strategyType,values,numberOfGroup);

        lenient().when(grouping.executeStrategy(values,numberOfGroup)).thenReturn(hasil);
    }

    @Test
    public void testToString() throws Exception {
        String strategyType = "group";

        lenient().when(strategyRepository.getStrategyByType(strategyType)).thenReturn(strategy);

        grouping.setStrategy(strategy);

        List<String> values = Arrays.asList(new String[]{"A","B","C"});
        int numberOfGroup = 2;

        String[] hasil = groupingService.chooseStrategy(strategyType,values,numberOfGroup);

        String expected = groupingService.toString(hasil);

        assertEquals(expected,groupingService.toString(hasil));
    }

    @Test
    public void testServiceOnGetStrategies() throws Exception {
        Iterable<Strategy> strategies = strategyRepository.getStrategy();

        assertEquals(groupingService.getStrategies(),strategies);
        lenient().when(groupingService.getStrategies()).thenReturn(strategies);
    }

    @Test
    public void testServiceOnGrouping() throws Exception {
        String strategyType = "group";

        int numberOfGroup = 2;

        String response = "";
        response += "Please register list of members first using command --register "
                + "<list_of_names_with_enter_to_separate_each_names>";

        RandomizerResult expected = new RandomizerResult(response);
        assertEquals(groupingService.grouping(strategyType,numberOfGroup),expected);
    }

}
