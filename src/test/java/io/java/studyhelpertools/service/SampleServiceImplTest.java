package io.java.studyhelpertools.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import io.java.studyhelpertools.model.SampleModel;
import io.java.studyhelpertools.repository.SampleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SampleServiceImplTest {

    @Mock
    private SampleRepository sampleRepository;

    @InjectMocks
    private SampleServiceImpl sampleService;

    private SampleModel sampleModel;

    @BeforeEach
    public void setUp() {
        sampleModel = new SampleModel("ID-00", "sample text");
    }

    @Test
    public void testCreateSampleModel() {
        when(sampleRepository.save(any(SampleModel.class))).thenReturn(sampleModel);

        SampleModel response = sampleService.createSampleModel(sampleModel);

        assertNotNull(response);
        assertEquals(sampleModel.getSampleId(), response.getSampleId());
        assertEquals(sampleModel.getDataString(), response.getDataString());

        verify(sampleRepository, times(1)).save(any(SampleModel.class));
    }

    @Test
    public void testGetSampleModelById() {
        when(sampleRepository.findBySampleId(sampleModel.getSampleId())).thenReturn(sampleModel);

        SampleModel response = sampleService.getSampleModelById(sampleModel.getSampleId());

        assertNotNull(response);
        assertEquals(sampleModel.getSampleId(), response.getSampleId());
        assertEquals(sampleModel.getDataString(), response.getDataString());

        verify(sampleRepository, times(1)).findBySampleId(anyString());
    }
}
