package io.java.studyhelpertools.service.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatisticsServiceImplTest {

    @InjectMocks
    private StatisticsServiceImpl statisticsService;

    private List<String> dataset;

    @BeforeEach
    public void setUp() {
        dataset = Arrays.asList("1", "2", "2", "2", "3");
    }

    @Test
    public void testMode() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(2));

        CalculatorResult result = statisticsService.findMode(dataset);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testMean() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(2));

        CalculatorResult result = statisticsService.findMean(dataset);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testMedian() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(2));

        CalculatorResult result = statisticsService.findMedian(dataset);

        assertEquals(expected.getResult(), result.getResult());
    }
}
