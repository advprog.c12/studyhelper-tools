package io.java.studyhelpertools.service.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TrigonometryServiceImplTest {

    @InjectMocks
    private TrigonometryServiceImpl trigonometryService;

    @Test
    public void testCsc() {
        CalculatorResult expected = new CalculatorResult("0.8509035245341184");

        CalculatorResult result = trigonometryService.csc(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testCos() {
        CalculatorResult expected = new CalculatorResult("0.5253219888177297");

        CalculatorResult result = trigonometryService.cos(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testCot() {
        CalculatorResult expected = new CalculatorResult("1.6197751905438615");

        CalculatorResult result = trigonometryService.cot(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testSec() {
        CalculatorResult expected = new CalculatorResult("1.9035944074044246");

        CalculatorResult result = trigonometryService.sec(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testSin() {
        CalculatorResult expected = new CalculatorResult("0.8509035245341184");

        CalculatorResult result = trigonometryService.sin(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testTan() {
        CalculatorResult expected = new CalculatorResult("1.6197751905438615");

        CalculatorResult result = trigonometryService.tan(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testToDegree() {
        CalculatorResult expected = new CalculatorResult("2578.3100780887044");

        CalculatorResult result = trigonometryService.toDegree(45);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testToRad() {
        CalculatorResult expected = new CalculatorResult("0.7853981633974483");

        CalculatorResult result = trigonometryService.toRad(45);

        assertEquals(expected.getResult(), result.getResult());
    }
}
