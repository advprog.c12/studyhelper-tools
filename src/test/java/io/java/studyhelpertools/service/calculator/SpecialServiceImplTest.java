package io.java.studyhelpertools.service.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SpecialServiceImplTest {

    @InjectMocks
    private SpecialServiceImpl specialService;

    @Test
    public void testCndf() {
        CalculatorResult expected = new CalculatorResult(Double.toString(1));

        CalculatorResult result = specialService.cndf(90);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testToBinary() {
        CalculatorResult expected = new CalculatorResult(Double.toString(1010));

        CalculatorResult result = specialService.toBinary(10);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testToDecimal() {
        CalculatorResult expected = new CalculatorResult(Double.toString(10));

        CalculatorResult result = specialService.toDecimal(1010);

        assertEquals(expected.getResult(), result.getResult());
    }
}
