package io.java.studyhelpertools.service.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.java.studyhelpertools.model.calculator.CalculatorResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ArithmeticServiceImplTest {

    @InjectMocks
    private ArithmeticServiceImpl arithmeticService;

    @Test
    public void testAdd() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(3));

        CalculatorResult result = arithmeticService.add(1, 2);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testSub() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(1));

        CalculatorResult result = arithmeticService.sub(3, 2);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testMul() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(6));

        CalculatorResult result = arithmeticService.mul(3, 2);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testDiv() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(3));

        CalculatorResult result = arithmeticService.div(6, 2);

        assertEquals(expected.getResult(), result.getResult());
    }

    @Test
    public void testMod() {
        CalculatorResult expected = new CalculatorResult(Integer.toString(0));

        CalculatorResult result = arithmeticService.mod(6, 2);

        assertEquals(expected.getResult(), result.getResult());
    }
}
