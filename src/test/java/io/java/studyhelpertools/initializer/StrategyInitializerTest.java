package io.java.studyhelpertools.initializer;

import static org.junit.jupiter.api.Assertions.assertTrue;

import io.java.studyhelpertools.core.randomizer.Strategy;
import io.java.studyhelpertools.repository.randomizer.StrategyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StrategyInitializerTest {
    @InjectMocks
    private StrategyInitializer strategyInitializer;

    @Mock
    private StrategyRepository strategyRepository;

    @BeforeEach
    public void setUp() {
        strategyRepository = new StrategyRepository();
        strategyInitializer.init();
    }

    @Test
    public void testStrategyRepoIsNotEmpty() {
        Iterable<Strategy> strategies = strategyRepository.getStrategy();
        assertTrue(strategies != null);
    }

}
