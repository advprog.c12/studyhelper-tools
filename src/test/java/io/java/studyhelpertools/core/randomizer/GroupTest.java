package io.java.studyhelpertools.core.randomizer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

import io.java.studyhelpertools.repository.randomizer.StrategyRepository;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GroupTest {

    private Class<?> groupClass;

    @InjectMocks
    private Group group;

    @Mock
    private StrategyRepository strategyRepository;

    @Mock
    private Grouping grouping;

    @BeforeEach
    public void setUp() throws Exception {
        String groupClassName = "io.java.studyhelpertools.core.randomizer.Group";
        groupClass = Class.forName(groupClassName);

        Strategy strategy = new Group();
        strategyRepository.addStrategy(strategy);
        grouping.setStrategy(strategy);
    }

    @Test
    public void testGroupOverrideExecuteMethod() throws Exception {
        Method execute = groupClass.getDeclaredMethod("execute", List.class,int.class);

        assertTrue(Modifier.isPublic(execute.getModifiers()));
        assertEquals("java.lang.String[]", execute.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGroupOverrideGetTypeMethod() throws Exception {
        Method execute = groupClass.getDeclaredMethod("getType");

        assertTrue(Modifier.isPublic(execute.getModifiers()));
        assertEquals("java.lang.String", execute.getGenericReturnType().getTypeName());
        assertEquals(0, execute.getParameterCount());
    }

    @Test
    public void testGroupOverrideShuffleMethod() throws Exception {
        Method execute = groupClass.getDeclaredMethod("shuffle",List.class);

        assertTrue(Modifier.isPublic(execute.getModifiers()));
        assertEquals("java.util.List<java.lang.String>",
                execute.getGenericReturnType().getTypeName());
        assertEquals(1, execute.getParameterCount());
    }

    @Test
    public void testGroupOnGetType() {
        assertEquals("group", group.getType());
    }

    @Test
    public void testGroupOnExecuteCommand() throws Exception {
        List<String> values = Arrays.asList(new String[]{"A","B","C"});
        int numberOfGroup = 3;

        String[] expected = group.execute(values,numberOfGroup);

        lenient().when(grouping.executeStrategy(values,numberOfGroup)).thenReturn(expected);
    }

    @Test
    public void testGroupOnExecuteCommandTidakHabisDibagi() throws Exception {
        List<String> values = Arrays.asList(new String[]{"A","B","C"});
        int numberOfGroup = 2;

        String[] expected = group.execute(values,numberOfGroup);

        lenient().when(grouping.executeStrategy(values,numberOfGroup)).thenReturn(expected);
    }
}
