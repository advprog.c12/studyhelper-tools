package io.java.studyhelpertools.core.randomizer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.java.studyhelpertools.repository.randomizer.StrategyRepository;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GroupingTest {
    @Mock
    private Strategy strategy;

    @Mock
    private StrategyRepository strategyRepository;

    @InjectMocks
    private Grouping grouping;

    private Class<?> groupingClass;

    @BeforeEach
    public void setUp() throws Exception {
        String groupingName = "io.java.studyhelpertools.core.randomizer.Grouping";
        groupingClass = Class.forName(groupingName);
    }

    @Test
    public void testGroupingHasExecuteStrategyMethod() throws Exception {
        Method executeStrategy = groupingClass.getDeclaredMethod("executeStrategy",
                List.class,int.class);

        assertTrue(Modifier.isPublic(executeStrategy.getModifiers()));
        assertEquals(2, executeStrategy.getParameterCount());
        assertEquals("java.lang.String[]", executeStrategy.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGroupingHasStrategySetter() throws Exception {
        Method setStrategy = groupingClass.getDeclaredMethod("setStrategy",
                Strategy.class);

        assertTrue(Modifier.isPublic(setStrategy.getModifiers()));
        assertEquals(1, setStrategy.getParameterCount());
        assertEquals("void", setStrategy.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGroupingHasStrategyGetter() throws Exception {
        Method getStrategy = groupingClass.getDeclaredMethod("getStrategy");

        assertTrue(Modifier.isPublic(getStrategy.getModifiers()));
        assertEquals(0, getStrategy.getParameterCount());
        assertEquals(Strategy.class, getStrategy.getGenericReturnType());
    }

    @Test
    public void testGroupingOnSetStrategy() {
        Strategy strategy = new Group();
        grouping.setStrategy(strategy);

        Strategy fetchedStrategy = grouping.getStrategy();

        assertThat(fetchedStrategy).isNotNull();
        assertThat(fetchedStrategy).isEqualTo(strategy);
    }

    @Test
    public void testGroupingOnExecuteStrategyCalledStrategyExecute() throws Exception {
        Strategy strategy = new Group();
        strategyRepository.addStrategy(strategy);
        grouping.setStrategy(strategy);

        List<String> a = new ArrayList<>();
        a.add("b");
        a.add("c");
        int d = 4;
        String[] hasil = grouping.executeStrategy(a,d);

        assertThat(hasil).isNotNull();

    }
}
