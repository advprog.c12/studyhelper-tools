package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

public class AddTest {

    private Addition addition;

    @BeforeEach
    public void setUp() throws Exception {
        addition = new Addition();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(10, addition.doCalculate(4,6));
        assertEquals(15, addition.doCalculate(7,8));
        assertEquals(21, addition.doCalculate(10,11));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("10", addition.toBeReturned(4,6));
        assertEquals("15", addition.toBeReturned(7,8));
        assertEquals("21", addition.toBeReturned(10,11));
    }
}
