package io.java.studyhelpertools.core.calculator.calculations.statistics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModeTest {

    private Mode mode;
    int [] datasetArrays;
    List<String> dataset;

    @BeforeEach
    public void setUp() throws Exception {
        mode = new Mode();
        datasetArrays = new int[]{2,3,2,3,2,2};
        dataset = Arrays.asList("2","3", "2", "3", "2", "2");
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(2, mode.doCalculate(datasetArrays));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("2", mode.toBeReturned(dataset));
    }
}
