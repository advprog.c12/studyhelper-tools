package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DivTest {

    private Division division;

    @BeforeEach
    public void setUp() throws Exception {
        division = new Division();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(2, division.doCalculate(20,10));
        assertEquals(3, division.doCalculate(45,15));
        assertEquals(4, division.doCalculate(100,25));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("2", division.toBeReturned(20,10));
        assertEquals("3", division.toBeReturned(45,15));
        assertEquals("4", division.toBeReturned(100,25));
    }
}
