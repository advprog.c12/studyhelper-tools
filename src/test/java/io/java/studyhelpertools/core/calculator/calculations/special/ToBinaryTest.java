package io.java.studyhelpertools.core.calculator.calculations.special;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ToBinaryTest {

    private ToBinary tbn;

    @BeforeEach
    public void setUp() throws Exception {
        tbn = new ToBinary();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(tbn.doCalculate(16), 10000.0);
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals(tbn.toBeReturned(16), "10000.0");
    }
}
