package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TangentTest {

    private Tangent tangent;

    @BeforeEach
    public void setUp() throws Exception {
        tangent = new Tangent();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(-6.405331196646276, tangent.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("-6.405331196646276", tangent.toBeReturned(30));
    }

    @Test
    public void testCalculateObjectReturnsUndefined() {
        assertEquals("Undefined", tangent.toBeReturned(90));
    }
}
