package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CosecantTest {

    private Cosecant cosecant;

    @BeforeEach
    public void setUp() throws Exception {
        cosecant = new Cosecant();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(-0.9880316240928618, cosecant.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("-0.9880316240928618", cosecant.toBeReturned(30));
    }

    @Test
    public void testCalculateObjectReturnsUndefined() {
        assertEquals("Undefined", cosecant.toBeReturned(0));
    }

}
