package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MulTest {

    private Multiplication multiplication;

    @BeforeEach
    public void setUp() throws Exception {
        multiplication = new Multiplication();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(24, multiplication.doCalculate(4,6));
        assertEquals(56, multiplication.doCalculate(7,8));
        assertEquals(110, multiplication.doCalculate(10,11));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("24", multiplication.toBeReturned(4,6));
        assertEquals("56", multiplication.toBeReturned(7,8));
        assertEquals("110", multiplication.toBeReturned(10,11));
    }
}
