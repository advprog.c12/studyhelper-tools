package io.java.studyhelpertools.core.calculator.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrigonoCalcImpTest {

    private Class<?> trigonoCalc;
    private TrigonoCalculatorImplementation trigonoFacade;

    @BeforeEach
    public void setup() throws Exception {
        trigonoFacade = new TrigonoCalculatorImplementation();
        trigonoCalc = TrigonoCalculatorImplementation.class;
    }

    @Test
    public void testPublic() {
        assertTrue(Modifier.isPublic(trigonoCalc.getModifiers()));
    }

    @Test
    public void testHasMethods() throws Exception {
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("csc", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("cos", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("cot", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("sec", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("sin", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("tan", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("toDegree", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(trigonoCalc.getDeclaredMethod("toRad", double.class).getModifiers()));
    }


    @Test
    public void testMethodReturns() {
        assertEquals(trigonoFacade.csc(30), "-0.9880316240928618");
        assertEquals(trigonoFacade.cos(30), "0.15425144988758405");
        assertEquals(trigonoFacade.cot(30), "-6.405331196646276");
        assertEquals(trigonoFacade.sec(30), "6.482921234962678");
        assertEquals(trigonoFacade.sin(30), "-0.9880316240928618");
        assertEquals(trigonoFacade.tan(30), "-6.405331196646276");
        assertEquals(trigonoFacade.toDegree(30), "1718.8733853924696");
        assertEquals(trigonoFacade.toRad(30), "0.5235987755982988");
    }

    @Test
    public void testMethodReturnsUndefined() {
        assertEquals(trigonoFacade.csc(0), "Undefined");
        assertEquals(trigonoFacade.sec(90), "Undefined");
        assertEquals(trigonoFacade.cot(0), "Undefined");
        assertEquals(trigonoFacade.tan(90), "Undefined");
    }
}
