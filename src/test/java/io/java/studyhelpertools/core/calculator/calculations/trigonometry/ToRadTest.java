package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ToRadTest {

    private ToRadians toRadians;

    @BeforeEach
    public void setUp() throws Exception {
        toRadians = new ToRadians();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(0.5235987755982988, toRadians.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("0.5235987755982988", toRadians.toBeReturned(30));
    }
}
