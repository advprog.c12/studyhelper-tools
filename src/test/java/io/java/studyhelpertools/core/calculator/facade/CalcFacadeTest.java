package io.java.studyhelpertools.core.calculator.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalcFacadeTest {

    private Class<?> calculator;
    private CalculatorImplementation calculatorFacade;

    @BeforeEach
    public void setup() throws Exception {
        calculatorFacade = new CalculatorImplementation();
        calculator = CalculatorImplementation.class;
    }

    @Test
    public void testPublic() {
        assertTrue(Modifier.isPublic(calculator.getModifiers()));
    }

    @Test
    public void testHasAdd() throws Exception {
        assertTrue(Modifier.isPublic(calculator.getDeclaredMethod("add", int.class, int.class).getModifiers()));
    }

    @Test
    public void testHasSub() throws Exception {
        assertTrue(Modifier.isPublic(calculator.getDeclaredMethod("sub", int.class, int.class).getModifiers()));
    }

    @Test
    public void testHasMul() throws Exception {
        assertTrue(Modifier.isPublic(calculator.getDeclaredMethod("mul", int.class, int.class).getModifiers()));
    }

    @Test
    public void testHasDiv() throws Exception {
        assertTrue(Modifier.isPublic(calculator.getDeclaredMethod("div", int.class, int.class).getModifiers()));
    }

    @Test
    public void testHasMod() throws Exception {
        assertTrue(Modifier.isPublic(calculator.getDeclaredMethod("mod", int.class, int.class).getModifiers()));
    }

    @Test
    public void testAddReturns() {
        assertEquals(calculatorFacade.add(1,2), "3");
    }


    @Test
    public void testSubReturns() {
        assertEquals(calculatorFacade.sub(1,2), "-1");
    }

    @Test
    public void testMulReturns() {
        assertEquals(calculatorFacade.mul(1,2), "2");
    }

    @Test
    public void testDivReturns() {
        assertEquals(calculatorFacade.div(1,2), "0");
    }

    @Test
    public void testModReturns() {
        assertEquals(calculatorFacade.mod(1,2), "1");
    }
}
