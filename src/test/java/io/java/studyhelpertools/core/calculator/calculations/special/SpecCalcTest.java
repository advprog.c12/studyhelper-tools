package io.java.studyhelpertools.core.calculator.calculations.special;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SpecCalcTest {

    private Class<?> specCalc;

    @BeforeEach
    public void setUp() throws Exception {
        specCalc = Class.forName("io.java.studyhelpertools.core.calculator.calculations.special.SpecCalc");
    }

    @Test
    public void testStatCalcIsPublic() {
        assertTrue(Modifier.isPublic(specCalc.getModifiers()));
    }

    @Test
    public void testStatCalcIsAnInterface() {
        assertTrue(Modifier.isInterface(specCalc.getModifiers()));
    }

    @Test
    public void testStatCalcToBeReturnedAbstract() throws Exception {
        assertTrue(Modifier.isAbstract(specCalc.getDeclaredMethod("toBeReturned", double.class).getModifiers()));
    }

    @Test
    public void testStatCalcToBeReturnedPublic() throws Exception {
        assertTrue(Modifier.isPublic(specCalc.getDeclaredMethod("toBeReturned", double.class).getModifiers()));
    }

    @Test
    public void testStatCalcToBeReturnedReturnsTheRightAmountOfParameter() throws Exception {
        assertEquals(specCalc.getDeclaredMethod("toBeReturned", double.class).getParameterCount(), 1);
    }
}
