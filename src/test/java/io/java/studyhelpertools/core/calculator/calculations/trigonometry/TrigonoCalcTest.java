package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrigonoCalcTest {

    private Class<?> trigonometricCalculator;

    @BeforeEach
    public void setUp() throws Exception {
        trigonometricCalculator = Class.forName(
                "io.java.studyhelpertools.core.calculator.calculations.trigonometry.TrigonoCalc");
    }

    @Test
    public void testCalculateIsPublic() {
        assertTrue(Modifier.isPublic(trigonometricCalculator.getModifiers()));
    }

    @Test
    public void testCalculateIsAnInterface() {
        assertTrue(Modifier.isInterface(trigonometricCalculator.getModifiers()));
    }

    @Test
    public void testCalculateDoCalculateAbstract() throws Exception {
        assertTrue(Modifier.isAbstract(trigonometricCalculator.getDeclaredMethod(
                "doCalculate", double.class).getModifiers()));
    }

    @Test
    public void testCalculateDoCalculatePublic() throws Exception {
        assertTrue(Modifier.isPublic(trigonometricCalculator.getDeclaredMethod(
                "doCalculate", double.class).getModifiers()));
    }

    @Test
    public void testCalculateDoCalculateReturnsTheRightAmountOfParameter() throws Exception {
        assertEquals(trigonometricCalculator.getDeclaredMethod(
                "doCalculate", double.class).getParameterCount(), 1);
    }

    @Test
    public void testCalculateToBeReturnedAbstract() throws Exception {
        assertTrue(Modifier.isAbstract(trigonometricCalculator.getDeclaredMethod(
                "toBeReturned", double.class).getModifiers()));
    }

    @Test
    public void testCalculateToBeReturnedPublic() throws Exception {
        assertTrue(Modifier.isPublic(trigonometricCalculator.getDeclaredMethod(
                "toBeReturned", double.class).getModifiers()));
    }

    @Test
    public void testCalculateToBeReturnedReturnsTheRightAmountOfParameter() throws Exception {
        assertEquals(trigonometricCalculator.getDeclaredMethod(
                "toBeReturned", double.class).getParameterCount(), 1);
    }
}
