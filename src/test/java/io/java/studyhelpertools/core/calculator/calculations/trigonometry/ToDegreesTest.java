package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ToDegreesTest {

    private ToDegrees toDegrees;

    @BeforeEach
    public void setUp() throws Exception {
        toDegrees = new ToDegrees();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(1718.8733853924696, toDegrees.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("1718.8733853924696", toDegrees.toBeReturned(30));
    }
}
