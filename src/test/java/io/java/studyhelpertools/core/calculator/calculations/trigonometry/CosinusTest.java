package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CosinusTest {

    private Class<?> classObject;
    private Cosinus cosinus;

    @BeforeEach
    public void setUp() throws Exception {
        classObject = Class.forName("io.java.studyhelpertools.core.calculator.calculations.trigonometry.Cosinus");
        cosinus = new Cosinus();
    }

    @Test
    public void testClass() {
        int classModifiers = classObject.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(cosinus instanceof TrigonoCalc);
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(0.15425144988758405, cosinus.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("0.15425144988758405", cosinus.toBeReturned(30));
    }

    @Test
    public void testToBeReturnedNumber90() {
        assertEquals("0", cosinus.toBeReturned(90));
    }
}
