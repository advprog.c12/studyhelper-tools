package io.java.studyhelpertools.core.calculator.calculations.special;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ToDecTest {

    private ToDec td;

    @BeforeEach
    public void setUp() throws Exception {
        td = new ToDec();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(td.doCalculate(1001), 9.0);
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals(td.toBeReturned(1001), "9.0");
    }
}
