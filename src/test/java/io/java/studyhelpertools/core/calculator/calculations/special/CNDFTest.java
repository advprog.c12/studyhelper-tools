package io.java.studyhelpertools.core.calculator.calculations.special;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CNDFTest {

    private CNDF cndf;

    @BeforeEach
    public void setUp() throws Exception {
        cndf = new CNDF();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(cndf.doCalculate(1.1), 0.8643338986562071);
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals(cndf.toBeReturned(1.1), "0.8643338986562071");
    }

    @Test
    public void testDoCalculateNegIsOne() {
        assertEquals(cndf.toBeReturned(-1.1), "0.13566610134379287");
    }
}
