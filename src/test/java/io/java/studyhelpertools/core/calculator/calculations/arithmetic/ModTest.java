package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModTest {

    private Modulo modulo;

    @BeforeEach
    public void setUp() throws Exception {
        modulo = new Modulo();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(9, modulo.doCalculate(19,10));
        assertEquals(14, modulo.doCalculate(44,15));
        assertEquals(1, modulo.doCalculate(101,25));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("9", modulo.toBeReturned(19,10));
        assertEquals("14", modulo.toBeReturned(44,15));
        assertEquals("1", modulo.toBeReturned(101,25));
    }
}
