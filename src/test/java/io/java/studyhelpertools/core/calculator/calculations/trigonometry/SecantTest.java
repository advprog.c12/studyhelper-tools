package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SecantTest {

    private Class<?> classObject;
    private Secant secant;

    @BeforeEach
    public void setUp() throws Exception {
        classObject = Class.forName("io.java.studyhelpertools.core.calculator.calculations.trigonometry.Secant");
        secant = new Secant();
    }

    @Test
    public void testClass() {
        int classModifiers = classObject.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(secant instanceof TrigonoCalc);
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(6.482921234962678, 1 / secant.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("6.482921234962678", secant.toBeReturned(30));
    }

    @Test
    public void testCalculateObjectReturnsUndefined() {
        assertEquals("Undefined", secant.toBeReturned(90));
        assertEquals("Undefined", secant.toBeReturned(270));
    }

}
