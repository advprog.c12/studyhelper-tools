package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SubTest {

    private Subtraction subtraction;

    @BeforeEach
    public void setUp() throws Exception {
        subtraction = new Subtraction();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(-2, subtraction.doCalculate(4,6));
        assertEquals(-1, subtraction.doCalculate(7,8));
        assertEquals(-1, subtraction.doCalculate(10,11));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("-2", subtraction.toBeReturned(4,6));
        assertEquals("-1", subtraction.toBeReturned(7,8));
        assertEquals("-1", subtraction.toBeReturned(10,11));
    }
}
