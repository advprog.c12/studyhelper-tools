package io.java.studyhelpertools.core.calculator.calculations.statistics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MedianTest {

    private Median median;
    int [] datasetArrays;
    List<String> dataset;

    @BeforeEach
    public void setUp() throws Exception {
        median = new Median();
        datasetArrays = new int[]{2,3,2,3,2,2};
        dataset = Arrays.asList("2","3", "2", "3", "2", "2");
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(2, median.doCalculate(datasetArrays));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("2", median.toBeReturned(dataset));
    }

    @Test
    public void testCalculateObjectReturnsTheRightStringOddLength() {
        List<String> dataset = Arrays.asList("1", "2", "3");
        assertEquals("2", median.toBeReturned(dataset));
    }
}
