package io.java.studyhelpertools.core.calculator.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SpecCalcImpTest {

    private Class<?> specCalc;
    private SpecialCalcImplementation specFacade;

    @BeforeEach
    public void setup() throws Exception {
        specFacade = new SpecialCalcImplementation();
        specCalc = SpecialCalcImplementation.class;
    }

    @Test
    public void testPublic() {
        assertTrue(Modifier.isPublic(specCalc.getModifiers()));
    }

    @Test
    public void testHasMethods() throws Exception {
        assertTrue(Modifier.isPublic(specCalc.getDeclaredMethod("toBinary", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(specCalc.getDeclaredMethod("toDecimal", double.class).getModifiers()));
        assertTrue(Modifier.isPublic(specCalc.getDeclaredMethod("cndf", double.class).getModifiers()));
    }


    @Test
    public void testMethodReturns() {
        assertEquals(specFacade.cndf(1.1), "0.8643338986562071");
        assertEquals(specFacade.toBinary(16), "10000.0");
        assertEquals(specFacade.toDecimal(1001), "9.0");
    }
}
