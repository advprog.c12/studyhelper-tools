package io.java.studyhelpertools.core.calculator.calculations.arithmetic;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.*;
import org.junit.jupiter.api.*;

public class CalculateTest {

    private Class<?> arithmeticCalculator;

    @BeforeEach
    public void setUp() throws Exception {
        arithmeticCalculator = Class.forName(
                "io.java.studyhelpertools.core.calculator.calculations.arithmetic.Calculate");
    }

    @Test
    public void testCalculateIsPublic() {
        assertTrue(Modifier.isPublic(arithmeticCalculator.getModifiers()));
    }

    @Test
    public void testCalculateIsAnInterface() {
        assertTrue(Modifier.isInterface(arithmeticCalculator.getModifiers()));
    }

    @Test
    public void testCalculateDoCalculateAbstract() throws Exception {
        assertTrue(Modifier.isAbstract(arithmeticCalculator.getDeclaredMethod(
                "doCalculate", int.class, int.class).getModifiers()));
    }

    @Test
    public void testCalculateDoCalculatePublic() throws Exception {
        assertTrue(Modifier.isPublic(arithmeticCalculator.getDeclaredMethod(
                "doCalculate", int.class, int.class).getModifiers()));
    }

    @Test
    public void testCalculateDoCalculateReturnsTheRightAmountOfParameter() throws Exception {
        assertEquals(arithmeticCalculator.getDeclaredMethod(
                "doCalculate", int.class, int.class).getParameterCount(), 2);
    }

    @Test
    public void testCalculateToBeReturnedAbstract() throws Exception {
        assertTrue(Modifier.isAbstract(arithmeticCalculator.getDeclaredMethod(
                "toBeReturned", int.class, int.class).getModifiers()));
    }

    @Test
    public void testCalculateToBeReturnedPublic() throws Exception {
        assertTrue(Modifier.isPublic(arithmeticCalculator.getDeclaredMethod(
                "toBeReturned", int.class, int.class).getModifiers()));
    }

    @Test
    public void testCalculateToBeReturnedReturnsTheRightAmountOfParameter() throws Exception {
        assertEquals(arithmeticCalculator.getDeclaredMethod(
                "toBeReturned", int.class, int.class).getParameterCount(), 2);
    }
}
