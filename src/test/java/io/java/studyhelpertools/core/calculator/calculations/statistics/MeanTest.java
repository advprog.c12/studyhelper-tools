package io.java.studyhelpertools.core.calculator.calculations.statistics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MeanTest {

    private Mean mean;
    int [] datasetArrays;
    List<String> dataset;

    @BeforeEach
    public void setUp() throws Exception {
        mean = new Mean();
        datasetArrays = new int[]{3,3,3,3,3};
        dataset = Arrays.asList("3","3", "3", "3", "3");
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(3, mean.doCalculate(datasetArrays));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("3", mean.toBeReturned(dataset));
    }
}
