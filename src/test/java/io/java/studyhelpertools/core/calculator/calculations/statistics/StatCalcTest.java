package io.java.studyhelpertools.core.calculator.calculations.statistics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StatCalcTest {

    private Class<?> statCalculator;

    @BeforeEach
    public void setUp() throws Exception {
        statCalculator = Class.forName("io.java.studyhelpertools.core.calculator.calculations.statistics.StatCalc");
    }

    @Test
    public void testStatCalcIsPublic() {
        assertTrue(Modifier.isPublic(statCalculator.getModifiers()));
    }

    @Test
    public void testStatCalcIsAnInterface() {
        assertTrue(Modifier.isInterface(statCalculator.getModifiers()));
    }

    @Test
    public void testStatCalcToBeReturnedAbstract() throws Exception {
        assertTrue(Modifier.isAbstract(statCalculator.getDeclaredMethod("toBeReturned", List.class).getModifiers()));
    }

    @Test
    public void testStatCalcToBeReturnedPublic() throws Exception {
        assertTrue(Modifier.isPublic(statCalculator.getDeclaredMethod("toBeReturned", List.class).getModifiers()));
    }

    @Test
    public void testStatCalcToBeReturnedReturnsTheRightAmountOfParameter() throws Exception {
        assertEquals(statCalculator.getDeclaredMethod("toBeReturned", List.class).getParameterCount(), 1);
    }
}
