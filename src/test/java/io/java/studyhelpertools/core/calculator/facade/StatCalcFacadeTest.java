package io.java.studyhelpertools.core.calculator.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StatCalcFacadeTest {

    private Class<?> statCalc;
    private StatCalcImplementation statCalcImplementation;
    int [] datasetArrays;
    List<String> dataset;

    @BeforeEach
    public void setup() throws Exception {
        statCalc = StatCalcImplementation.class;
        statCalcImplementation = new StatCalcImplementation();
        datasetArrays = new int[]{2,3,2,3,2,2};
        dataset = Arrays.asList("2","3", "2", "3", "2", "2");
    }

    @Test
    public void testPublic() {
        assertTrue(Modifier.isPublic(statCalc.getModifiers()));
    }

    @Test
    public void testHasMethods() throws Exception {
        assertTrue(Modifier.isPublic(statCalc.getDeclaredMethod("findMode", List.class).getModifiers()));
        assertTrue(Modifier.isPublic(statCalc.getDeclaredMethod("findMean", List.class).getModifiers()));
        assertTrue(Modifier.isPublic(statCalc.getDeclaredMethod("findMedian", List.class).getModifiers()));
    }


    @Test
    public void testMethodReturns() {
        assertEquals(statCalcImplementation.findMode(dataset), "2");
        assertEquals(statCalcImplementation.findMean(dataset), "2");
        assertEquals(statCalcImplementation.findMedian(dataset), "2");
    }



}
