package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CotangentTest {

    private Class<?> classObject;
    private Cotangent cotangent;

    @BeforeEach
    public void setUp() throws Exception {
        classObject = Class.forName("io.java.studyhelpertools.core.calculator.calculations.trigonometry.Cotangent");
        cotangent = new Cotangent();
    }

    @Test
    public void testClass() {
        int classModifiers = classObject.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(cotangent instanceof TrigonoCalc);
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(-6.405331196646276, cotangent.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("-6.405331196646276", cotangent.toBeReturned(30));
    }

    @Test
    public void testCalculateObjectReturnsUndefined() {
        assertEquals("Undefined", cotangent.toBeReturned(0));
    }

    @Test
    public void testDoCalculateInput90() {
        assertEquals("Undefined", cotangent.toBeReturned(90));
    }
}
