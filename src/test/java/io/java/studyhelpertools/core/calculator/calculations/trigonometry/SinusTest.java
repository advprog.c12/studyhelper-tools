package io.java.studyhelpertools.core.calculator.calculations.trigonometry;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SinusTest {

    private Sinus sinus;

    @BeforeEach
    public void setUp() throws Exception {
        sinus = new Sinus();
    }

    @Test
    public void testCalculateObjectReturnsTheRightAmount() {
        assertEquals(-0.9880316240928618, sinus.doCalculate(30));
    }

    @Test
    public void testCalculateObjectReturnsTheRightString() {
        assertEquals("-0.9880316240928618", sinus.toBeReturned(30));
    }
}
