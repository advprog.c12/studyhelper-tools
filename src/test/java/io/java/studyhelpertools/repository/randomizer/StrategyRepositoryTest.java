package io.java.studyhelpertools.repository.randomizer;

import static org.assertj.core.api.Assertions.assertThat;

import io.java.studyhelpertools.core.randomizer.Group;
import io.java.studyhelpertools.core.randomizer.Strategy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StrategyRepositoryTest {
    private StrategyRepository strategyRepository;

    @BeforeEach
    public void setUp() {
        strategyRepository = new StrategyRepository();
    }

    @Test
    public void testStrategyRepositoryOnAddingStrategy() {
        strategyRepository.addStrategy(new Group());
        Iterable<Strategy> strategies = strategyRepository.getStrategy();

        assertThat(strategies).isNotEmpty();
    }

    @Test
    public void testStrategyRepositoryOnFindingStrategyByType() {
        Strategy strategy = new Group();
        strategyRepository.addStrategy(strategy);

        Strategy fetchedStrategy = strategyRepository.getStrategyByType(strategy.getType());

        assertThat(fetchedStrategy).isNotNull();
        assertThat(fetchedStrategy.getType()).isEqualTo(strategy.getType());
    }
}
